module gitlab.com/renty-dev/gorenty

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/trifles v0.0.0-20200323201526-dd97f9abfb48
	github.com/getsentry/sentry-go v0.6.0
	github.com/kurin/blazer v0.5.3
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.3.0 // indirect
	github.com/mitchellh/mapstructure v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prisma/prisma-client-lib-go v0.0.0-20181017161110-68a1f9908416
	github.com/secrethub/secrethub-go v0.28.0
)
