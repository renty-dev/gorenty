# GoRenty - The Awesome Renty Golang Methods Library

We use this repo as a private go module collection

## Setup :

For UN*X like OSes :

- Create a personnal acess token for your gitlab profile [here](https://gitlab.com/profile/personal_access_tokens)

- You could set an `expiration date` if your kind of paranoid

- Give it only the `read_repository` right as it wont do much than that

- After hitting `Create personal acess token` copy the given value to clipboard

- Go back to your IDE ; Note : I, @dave-lopeur recommend you to save it in a .env file somewhere in the repo

- Then open or create a file called `/home/$USER/.netrc` and insert the below lines 

```
machine gitlab.com
  login yourgitlabusername
  password youracesstoken
```

- Finally go back to your Go Code and enter `go env -w GOPRIVATE=gitlab.com/renty-dev`

- Success !! You could now use "gitlab.com/renty-dev/gorenty" utilities in your code 

For windows :

- Follow the previous steps to get access token

- See this [SO answer](https://stackoverflow.com/a/45936697/13138128) to get the private package in go code