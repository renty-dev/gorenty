#!/usr/bin/env bash

echo -e "\n🔧  Applying fix for Uuid type issue (see https://github.com/prisma/prisma/issues/4438 )"
sed -i '15 a type Uuid string\n' ./generated/prisma/prisma.go
echo -e "\n🔧 Adding Apollo Federation Required interface User.IsEntity() and RentOffer.IsEntity()"
sed -i '/^type DeviceExec struct*/i func (User) IsEntity() {}\n' ./generated/prisma/prisma.go
sed -i '/^type PriceExec struct*/i func (RentOffer) IsEntity() {}\n' ./generated/prisma/prisma.go
echo -e "\nEt voila ! 🚀\n"