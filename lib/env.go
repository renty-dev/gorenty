package lib

import "os"

const (
	// ServerStageDev is the dev stage value
	ServerStageDev = "dev"
	// ServerStageStaging is the staging (preprod) stage value
	ServerStageStaging = "staging"
	// ServerStageProduction is the production stage value
	ServerStageProduction = "prod"
)

// GetDefVal get value for envar key or set default val
func GetDefVal(key, val string) string {
	if ok := os.Getenv(key); ok != "" {
		val = ok
	}
	return val
}
