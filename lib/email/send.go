package email

import (
	"fmt"
	"log"
	"net/smtp"
	"os"
	"strings"
	"time"

	"gitlab.com/renty-dev/gorenty/lib"

	"github.com/dgryski/trifles/uuid"
)

// SMTPAuth represent the smtp server credentials
type SMTPAuth struct {
	Host     string `toml:"smtp_host"`
	Port     string `toml:"smtp_port"`
	UserName string `toml:"smtp_user"`
	Password string `toml:"smtp_pass"`
}

// Email represent a mail to send
type Email struct {
	To      []string
	From    string
	Subject string
	Message string
}

var smtpAuth = SMTPAuth{}

// Init setup smtp config
func Init() {
	smtpAuth.Host = os.Getenv("SMTP_HOST")
	smtpAuth.Port = os.Getenv("SMTP_PORT")
	smtpAuth.UserName = os.Getenv("SMTP_USER")
	smtpAuth.Password = os.Getenv("SMTP_PASSWORD")
	if smtpAuth.Host == "" || smtpAuth.Port == "" ||
		smtpAuth.UserName == "" || smtpAuth.Password == "" {
		err := fmt.Sprintf("Cannot start without SMTP envaars")
		lib.LogError("sendmail/init", err)
		log.Fatal(err)
	}
}

// SendEmail send and email with go smtp package
func SendEmail(email Email) chan error {
	var err error
	const CRLF = "\r\n"
	const rfc2822 = "Mon, 02 Jan 2006 15:04:05 -0700"
	var errs = make(chan error, 1)
	Init()
	defer close(errs)
	auth := smtp.CRAMMD5Auth(smtpAuth.UserName, smtpAuth.Password)
	mime := "MIME-version: 1.0;" + CRLF
	tomsg := "To: " + strings.Join(email.To, ",") + CRLF
	frommsg := "From: " + email.From + CRLF
	subjectmsg := "Subject: " + email.Subject + CRLF
	date := "Date: " + time.Now().Format(rfc2822) + CRLF
	messageID := "Message-ID: <" + uuid.UUIDv4() + "@" + os.Getenv("MAIL_DOMAIN") + ">" + CRLF
	contentTransferEncoding := "Content-Transfer-Encoding: 8bit" + CRLF
	contentType := "Content-Type: text/html; charset=\"UTF-8\";" + CRLF
	fullBody := fmt.Sprintf("%s%s%s%s%s%s%s%s%s",
		date,
		tomsg,
		frommsg,
		subjectmsg,
		messageID,
		mime,
		contentTransferEncoding,
		contentType,
		email.Message,
	)
	logmsg := fmt.Sprintf("Will send mail size %v via %s:%s from %s to %s",
		len(email.Message),
		smtpAuth.Host,
		smtpAuth.Port,
		email.From,
		email.To,
	)
	lib.LogInfo("SendEmail", logmsg)
	if err = smtp.SendMail(
		smtpAuth.Host+":"+smtpAuth.Port,
		auth,
		smtpAuth.UserName,
		email.To,
		[]byte(fullBody)); err != nil {
		lib.LogError("email/Send", err.Error())
		errs <- err
	}
	return errs
}
