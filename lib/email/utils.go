package email

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"

	"gitlab.com/renty-dev/gorenty/generated/prisma"
	"gitlab.com/renty-dev/gorenty/lib"

	"github.com/dgryski/trifles/uuid"
)

// Registration return an email create input after sending a confirmation email
func Registration(db *prisma.Client, username, emailinput string) *prisma.EmailCreateOneInput {
	var err error
	var exist bool
	var nemail *Email
	var mailtpl *prisma.EmailTemplate
	var tpltype = prisma.EmailTypeRegistration
	if exist, err = Exist(db, emailinput); err != nil || exist {
		if exist {
			err = fmt.Errorf("email already registered")
		}
		lib.LogError("EmailRegistration", err.Error())
		return nil
	}
	if validhost := IsValidHost(emailinput); !validhost {
		return nil
	}
	if mailtpl, err = db.EmailTemplate(prisma.EmailTemplateWhereUniqueInput{
		Type: &tpltype,
	}).Exec(context.Background()); err != nil {
		lib.LogError("EmailRegistration/GetMailTemplate", err.Error())
		return nil
	}
	confirmToken := uuid.UUIDv4()
	confirmLink := os.Getenv("URL") + "/checkemail/" + confirmToken
	nemail = TemplateToEmail(mailtpl, ValidationEmailParams{
		Username:     username,
		ConfirmLink:  confirmLink,
		SupportEmail: os.Getenv("SUPPORT_EMAIL"),
	})
	nemail.To = []string{username + " <" + emailinput + ">"}
	go SendEmail(*nemail)
	toUUID := prisma.Uuid(confirmToken)
	return &prisma.EmailCreateOneInput{
		Create: &prisma.EmailCreateInput{
			Value:            emailinput,
			ConfirmationCode: &toUUID,
		},
	}
}

// IsValidHost check a domain for existence and their mx records
func IsValidHost(email string) bool {
	var err error
	var ok bool
	var mx []*net.MX
	hostInfo := strings.Split(email, "@")
	// Check
	if mx, err = net.LookupMX(hostInfo[1]); err != nil {
		lib.LogError("mutation/RegisterEmail", err.Error())
		return false
	}
	for _, rec := range mx {
		if rec != nil {
			ok = true
			fmt.Printf("Got MX : %+v\n", *rec)
			break
		}
	}
	return ok
}

// Exist check if an email is already registered
func Exist(db *prisma.Client, email string) (bool, error) {
	var (
		err   error
		exist bool
	)
	if exist, err = db.Email(prisma.EmailWhereUniqueInput{
		Value: &email,
	}).Exists(context.Background()); err != nil {
		if err.Error() == prisma.ErrNoResult.Error() {
			return exist, nil
		}
		return exist, err
	}
	return exist, err
}
