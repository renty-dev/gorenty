package email

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"text/template"

	"gitlab.com/renty-dev/gorenty/generated/prisma"
	"gitlab.com/renty-dev/gorenty/lib"
)

// ValidationEmailParams is the needed param to fill a registration email template
type ValidationEmailParams struct {
	Username     string
	ConfirmLink  string
	SupportEmail string
}

// FillTemplate return a template string filled with the required variables
func FillTemplate(tofill string, params interface{}) *string {
	var err error
	var t = template.New("confirm_email")
	var tpl bytes.Buffer
	if t, err = template.New("template").Parse(tofill); err != nil {
		fmt.Printf("Failed to parse template : %s\n", err.Error())
		return nil
	}
	if err = t.Execute(&tpl, params); err != nil {
		return nil
	}
	result := tpl.String()
	return &result
}

// TemplateToEmail fill an email template with params and return an email ready to send
func TemplateToEmail(tpl *prisma.EmailTemplate, params interface{}) *Email {
	var nemail Email
	if strings.Contains(tpl.Subject, "{{") {
		if filled := FillTemplate(tpl.Subject, params); filled != nil {
			nemail.Subject = *filled
		} else {
			lib.LogError("email/TemplateToEmail", "Error filling subject")
		}
	} else {
		nemail.Subject = tpl.Subject
	}
	if strings.Contains(tpl.Body, "{{") {
		if filled := FillTemplate(tpl.Body, params); filled != nil {
			nemail.Message = *filled
		} else {
			lib.LogError("email/TemplateToEmail", "Error filling body")
		}
	} else {
		nemail.Message = tpl.Body
	}
	if tpl.From != nil && strings.Contains(*tpl.From, "{{") {
		if filled := FillTemplate(*tpl.From, params); filled != nil {
			nemail.From = *filled
		} else {
			lib.LogError("email/TemplateToEmail", "Error filling from")
		}
	} else {
		nemail.From = os.Getenv("SMTP_FROM")
	}
	return &nemail
}
