package chat

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
	"time"
)

var (
	// WebhookURL is the mattermost slack like chat webhook uri
	WebhookURL = os.Getenv("WEBHOOK_URL")
)

// Mattermost is the wrapper around http client
type Mattermost struct {
	URL    string
	Client *http.Client
}

// Message is the mattermost message format
type Message struct {
	Text        string  `json:"text"`
	Channel     *string `json:"channel,omitempty"`
	Username    *string `json:"username,omitempty"`
	IconURL     *string `json:"icon_url,omitempty"`
	IconEmoji   *string `json:"icon_emoji,omitempty"`
	Attachments *string `json:"attachments,omitempty"`
}

// DefaultClient is the default mattermost client to send webhook with
var DefaultClient = &Mattermost{}

// Load setup Mattermost default client
func Load() {
	DefaultClient.URL = WebhookURL
	DefaultClient.Client = &http.Client{
		Timeout: 12 * time.Second,
	}
}

// Send a message to dedicated channel on Mattermost instance
func (m *Mattermost) Send(msg Message) chan error {
	var err error
	var errs = make(chan error, 1)
	var req *http.Request
	var bodyByte []byte

	defer close(errs)
	if bodyByte, err = json.Marshal(msg); err != nil {
		errs <- err
		return errs
	}
	if req, err = http.NewRequest("POST", m.URL, bytes.NewReader(bodyByte)); err != nil {
		errs <- err
		return errs
	}
	req.Header.Set("Content-Type", "application/json")
	if _, err = m.Client.Do(req); err != nil {
		errs <- err
	}
	return errs
}
