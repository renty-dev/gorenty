package chat

var (
	// RegistrationsChan is the Mattermost channel name for user registration live feed
	RegistrationsChan = "users-registrations"
	// RegistrationsUsername ...
	RegistrationsUsername = "RentyRegistrations"
	// RegistrationsEmoji ...
	RegistrationsEmoji = ":handshake:"
)
