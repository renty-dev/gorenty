package authy

import (
	"context"
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"gitlab.com/renty-dev/gorenty/lib"

	"log"

	"github.com/dgrijalva/jwt-go"
	"github.com/secrethub/secrethub-go/pkg/secrethub"
)

type key int

var contextKey key

// Auth is the struct containing user infos
type Auth struct {
	Valid    bool
	Username *string
	UserRole *string
	Context  *string
}

// Config is the required config for Authy lib to work
type Config struct {
	JwtIssuer              string `env:"JWT_ISSUER"`
	JwtSigningPubKeyString string `env:"SIGN_KEY"`
	SecretHubPubKeyPath    string `env:"SCH_PUBKEY_PATH"`
	SecretHubCredential    string `env:"SECRETHUB_CREDENTIAL"`
	JwtSigningPubKey       *rsa.PublicKey
}

var (
	// IsBearer check if Authorization header contain the bearer string
	IsBearer = regexp.MustCompile(`(?i)bearer`)
	// ErrBadAlgo is returned when signing alg in jwt header is not the one
	// we sign this token with
	ErrBadAlgo = fmt.Errorf("invalid alg value")
	// ErrBadToken is explicit
	ErrBadToken = fmt.Errorf("bad token")
)

// DefaultClient is the Authy module client
var DefaultClient *Config

// Init load the public keys from secrethub to verify incoming jwt tokens
// Must be called early on the app startup, exit if error occurred.
func Init() {
	var (
		err    error
		client *secrethub.Client
		conf   Config
	)
	conf.JwtIssuer = lib.GetDefVal("JWT_ISSUER", "renty-dev")
	conf.JwtSigningPubKeyString = lib.GetDefVal("SIGN_KEY", "./my.key.pub")
	conf.SecretHubPubKeyPath = lib.GetDefVal("SCH_PUBKEY_PATH", "Renty/backend/my.key.pub")
	conf.SecretHubCredential = lib.GetDefVal("SECRETHUB_CREDENTIAL", "")
	if client, err = secrethub.NewClient(); err != nil {
		if os.Getenv("STAGE") != lib.ServerStageDev {
			log.Fatal(err)
		}
	}
	if os.Getenv("STAGE") == lib.ServerStageDev {
		pubkeyfile, _ := ioutil.ReadFile(conf.JwtSigningPubKeyString)
		conf.JwtSigningPubKeyString = string(pubkeyfile)
	} else if conf.JwtSigningPubKeyString, err = client.Secrets().ReadString(
		conf.SecretHubPubKeyPath,
	); err != nil {
		log.Fatal(err)

	}
	if conf.JwtSigningPubKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(conf.JwtSigningPubKeyString)); err != nil {
		log.Fatal(err)
	}
	DefaultClient = &conf
}

// NewContext returns a new Context that carries value u.
func NewContext(ctx context.Context, u *Auth) context.Context {
	return context.WithValue(ctx, contextKey, u)
}

// FromContext returns the User value stored in ctx, if any.
func FromContext(ctx context.Context) (*Auth, bool) {
	u, ok := ctx.Value(contextKey).(*Auth)
	return u, ok
}

// CheckJWTSign return the signing bytes for the jwt lib to validate token signature
func CheckJWTSign(token *jwt.Token) (interface{}, error) {
	var err error
	if token.Method.Alg() != jwt.SigningMethodRS256.Name {
		err = ErrBadAlgo
	}
	return DefaultClient.JwtSigningPubKey, err
}

// ParseToken parse and verify the jwt token granted by Authy service
func ParseToken(rawtoken string) (*Auth, error) {
	var (
		err    error
		claims jwt.StandardClaims
		parsed *jwt.Token
		infos  Auth
	)
	if IsBearer.Match([]byte(rawtoken)) {
		rawtoken = IsBearer.ReplaceAllString(rawtoken, "")
		rawtoken = strings.Trim(rawtoken, " ")
	}
	if parsed, err = jwt.ParseWithClaims(rawtoken, &claims, CheckJWTSign); err != nil {
		// lib.LogError("authy/ParseToken", err.Error())
		return nil, ErrBadToken
	}
	if parsed.Valid {
		userInfos := strings.Split(claims.Subject, ":")
		subjectInfos := strings.Split(claims.Audience, ":")
		if len(userInfos) > 0 && len(subjectInfos) > 0 {
			infos.Username = &userInfos[0]
			infos.UserRole = &userInfos[1]
			infos.Context = &subjectInfos[1]
			infos.Valid = true
		} else {
			// logmsg := fmt.Sprintf("Weird token : %+v\n", claims)
			// lib.LogError("auty/ParseToken", logmsg)
			return nil, ErrBadToken
		}
	}
	return &infos, err
}
