package lib

import (
	"fmt"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

// TMSTP is ...
var TMSTP = time.Now().Format(time.RFC3339)

// InitSentry load Sentry error reporting tool
func InitSentry() {
	// Init Sentry.io errors reporting service
	if err := sentry.Init(sentry.ClientOptions{
		Dsn:         os.Getenv("SENTRY_DSN"),
		DebugWriter: os.Stderr,
		Debug:       true,
		Environment: os.Getenv("STAGE"),
		Release:     os.Getenv("RELEASE"),
		SampleRate:  0.5,
	}); err != nil {
		LogError("main", err.Error())
		os.Exit(1)
	}
}

// LogInfo log info with timestamp on stdout
func LogInfo(orig, message string) string {
	logmsg := fmt.Sprintf("[%s] 💡 Info(%s): %s\n", TMSTP, orig, message)
	fmt.Fprint(os.Stdout, logmsg)
	return logmsg
}

// LogError log error with timestamp on stderr
func LogError(orig, message string) string {
	logmsg := fmt.Sprintf("[%s] 🚨  Error(%s): %s\n", TMSTP, orig, message)
	if os.Getenv("STAGE") != ServerStageDev {
		sentry.CaptureMessage(logmsg)
	}
	fmt.Fprint(os.Stderr, logmsg)
	return logmsg
}
