package dabucket

import (
	"context"
	"fmt"
	"io"
	"net/url"
	"os"
	"time"

	"github.com/kurin/blazer/b2"
)

var (
	daBucket *b2.Bucket
	// ErrNilContext is returned when a received context is nil
	ErrNilContext = fmt.Errorf("context is nil cannot keep going")
	// ErrFailedToLoad is returned when a bucket object failed to be loaded
	ErrFailedToLoad = fmt.Errorf("failed to load bucket Object")
	// ErrServerError is returned when an unknow error happen
	ErrServerError = fmt.Errorf("sorry something bad happen try again later")
)

// BucketFile is a wrapper to save file from Graphql Upload scalar
type BucketFile struct {
	Buffer io.Reader
	Fpath  string
	Fname  string
	Fsize  int64
}

// Init setup the global b2 client (exit if error occurred)
func Init() {
	var err error
	var client *b2.Client
	var ctx = context.Background()
	if client, err = b2.NewClient(
		ctx,
		os.Getenv("B2_ACCOUNT"),
		os.Getenv("B2_KEY"),
	); err != nil {
		// lib.LogError("dabucket/init", err.Error())
		// if lib.ServerConf.Stage != lib.DevStage {
		// 	os.Exit(1)
		// }
		os.Exit(1)
	}
	if daBucket, err = client.Bucket(ctx, os.Getenv("B2_BUCKETNAME")); err != nil {
		// lib.LogError("dabucket/init", err.Error())
		// if lib.ServerConf.Stage != lib.DevStage {
		// 	os.Exit(1)
		// }
		os.Exit(1)
	}
}

// GetSignedURL return a signed url from BackBlaze to prepare a file upload
func GetSignedURL(ctx context.Context, file *BucketFile) (*string, error) {
	var err error
	var sURL *url.URL
	var obj *b2.Object
	var toString string

	if ctx == nil {
		return nil, ErrNilContext
	}
	obj = daBucket.Object(file.Fpath)
	if sURL, err = obj.AuthURL(ctx, 10*time.Minute, ""); err != nil {
		return nil, ErrServerError
	}
	toString = sURL.String()
	return &toString, err
}

// DeleteFile delete a file in bucket
func DeleteFile(ctx context.Context, fpath string) error {
	var err error
	var obj *b2.Object

	if ctx == nil {
		return ErrNilContext
	}
	obj = daBucket.Object(fpath)
	if err = obj.Delete(ctx); err != nil {
		// lib.LogError("dabucket/DeletFile", err.Error())
		return fmt.Errorf("object [%s] is not in bucket", fpath)
	}
	return err
}

// CopyFile upload a file to B2 cloud storage bucket
func CopyFile(ctx context.Context, file BucketFile) (*string, error) {
	var obj *b2.Object

	if ctx == nil {
		return nil, ErrNilContext
	}
	if obj = daBucket.Object(file.Fpath); obj == nil {
		return nil, ErrFailedToLoad
	}
	w := obj.NewWriter(ctx)
	if _, err := io.Copy(w, file.Buffer); err != nil {
		if closeerr := w.Close(); closeerr != nil {
			// lib.LogError("lib/dabucket", err.Error())
		}
		return nil, err
	}
	if err := w.Close(); err != nil {
		return nil, err
	}
	fileURL := obj.URL()
	return &fileURL, nil
}
